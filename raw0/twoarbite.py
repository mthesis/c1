import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import isfile

from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from cauc import cauc

from scipy.optimize import minimize as mini
from ageta import gs



fil="evalb"

key=""




f=np.load(fil+".npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]



def partiate(p,c,y,d1l,d1h,d2l,d2h):
  return p[:,d1l:d1h,d2l:d2h],c[:,d1l:d1h,d2l:d2h],y
def partiate2(p,c,y,d):
  ap=np.reshape(p,(p.shape[0],p.shape[1]*p.shape[2]),order="F")
  ac=np.reshape(c,(c.shape[0],c.shape[1]*c.shape[2]),order="F")
  return ap[:,d],ac[:,d],y
def partiate3(p,c,y,mults):
  mult=np.reshape(mults,(4,4))
  pp=p*mult
  cc=c*mult
  return pp,cc,y

def test(p,c,y):
  ac=cauc(p=p,c=c,y=y)
  return ac["auc"],ac["e30"],p.shape,ac

def testmult(mults):
  auc,e30,shape,data=test(*partiate3(p,c,y,mults))
  return -auc

  

mults=[1.0,1.0,1.0,1.0]
multsq=[]
for i in range(gs):
  multsq.append(mults)
mults=multsq

#auc,e30,shape,data=test(*partiate(p,c,y,d1l,d1h,d2l,d2h))

#print(testmult(mults))
#exit()

mults=mini(testmult,x0=mults,options={"disp":True}, method='Nelder-Mead', tol=1e-2)

print(mults)
print(testmult(mults.x))













