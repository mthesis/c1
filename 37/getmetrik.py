import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model
import json


from createmodel import objects as o
from advload import *


for mi in ["b","a"]:

  model=aload("modelw"+mi+".h5")



  ll=model.layers


  wei=[]
  nam=[]
  met=[]

  for l in ll:
    if l._name.find("gtopk")>-1:
      w=l.get_weights()
      acm=[]
      for e in w[0]:
        acm.append(float(e[0]))
      met.append(acm)
    wei.append(l.get_weights())
    nam.append(l._name)


  np.savez_compressed("metrik"+mi,wei=wei,nam=nam,met=met)


  print(np.array(met))

 
  with open("metrik"+mi+".json","w") as f:
    f.write(json.dumps(met)) 
  
