import numpy as np
import matplotlib.pyplot as plt

from cauc import cauc




n=10000
maxgs=100



f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:maxgs,1:3]
x=np.reshape(x,(int(x.shape[0]),int(x.shape[1]),-1))
y=f["y"][:n]
del f

f=np.load("evalb.npz")
x=f["c"]
y=f["y"]


x=x[:,:,1:3]

print(np.mean(x[:,-1,:]))


def getforgs(gs):
  acx=x[:,-gs:,:]
  assert int(acx.shape[1])==gs
  return cauc(p=acx,c=np.zeros_like(acx),y=y)




gss=np.arange(1,6,1)
aucs=[]
for gs in gss:
  aucs.append(getforgs(gs=gs)["auc"])
  print("did",gs)

plt.plot(gss,aucs)
plt.show()








