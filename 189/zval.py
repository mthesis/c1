import numpy as np

from cauc import cauc,caucd

f=np.load("evalb.npz")

#q=cauc(p=f["p"],c=f["c"],y=f["y"])
#print(q["auc"])

p=f["p"]
c=f["c"]
y=f["y"]

p=p[:,:,1:3]
c=c[:,:,1:3]

d=(p-c)**2

#d=p
#d=c



d=np.reshape(d,(int(d.shape[0]),int(d.shape[1]),-1))

d=d[:,:6,:]


while len(d.shape)>1:
  d=np.mean(np.abs(d),axis=-1)

ds=d[np.where(y>0.5)]
db=d[np.where(y<0.5)]

print("sign",np.mean(ds))
print("back",np.mean(db))

q=caucd(d=d,y=y)

print("auc",q["auc"])
print("e30",1/(q["e30"]+0.000001))






