import numpy as np
import matplotlib.pyplot as plt

from qstat import *


folds=["169","166","170","167","171","168","172"]
gss=[3,4,5,6,7,8,9]



losses=[]
aucs=[]
e30s=[]
arbs=[]
invarbs=[]
twoarbs=[]

for i in range(len(gss)):
  gs=gss[i]
  fold=folds[i]+"/"
  loss=minloss(pref=fold)
  losses.append(loss)
  auc,e30=roc(pref=fold)
  aucs.append(auc)
  e30s.append(e30)
  acarb,_=arb(pref=fold)
  arbs.append(acarb)
  acinvarb,_=invarb(pref=fold)
  invarbs.append(acinvarb)
  actwoarb,_=twoarb(pref=fold)
  twoarbs.append(actwoarb)


plt.plot(gss,aucs,label="basic")
plt.plot(gss,arbs,label="feature opt")
plt.plot(gss,invarbs,label="particle opt")
plt.plot(gss,twoarbs,label="opt both")


losses=np.array(losses)
losses-=np.min(losses)
losses/=np.max(losses)
losses*=0.18
losses+=0.76
plt.plot(gss,losses,label="loss")

plt.legend()
plt.ylim([0.75,0.95])

plt.show()




