import numpy as np
import matplotlib.pyplot as plt

from zcreate import zageta
from cauc import caucd

gss=[2,3,4,5,6,7,8,9,10,11,12,13,14,15,20,25,30,35,40,45,50]
gss=np.arange(2,200)

n=10000


f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:np.max(gss),:]
y=f["y"][:n]
del f

aucs=[]

for gs in gss:
  model=zageta(gs)
  ax=x[:,:gs,:]
  vx=model.predict(ax)

  #ax=ax[:,:,1:3]
  vx=vx[:,:,1:3]
 
  vx=np.mean(np.abs(vx),axis=(1,2))

 
  q=caucd(d=vx,y=y)
  
  aucs.append(q["auc"])
  for i in range(100):print("did",gs)




np.savez_compressed("zout",gss=gss,aucs=aucs)


plt.plot(gss,aucs)


plt.xlabel("gs")
plt.ylabel("auc")

plt.savefig("imgs/zero.png",format="png")
plt.savefig("imgs/zero.pdf",format="pdf")


plt.show()








