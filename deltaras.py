import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import isfile

from matplotlib.colors import ListedColormap, LinearSegmentedColormap


fol1="32"
fol2="33"


fol1="00"
fol2="14"


if len(sys.argv)>1:
  fol1=sys.argv[1]
if len(sys.argv)>2:
  fol2=sys.argv[2]



f1=np.load(fol1+"/raster.npz")
f2=np.load(fol2+"/raster.npz")

a1=f1["aucs"]
a2=f2["aucs"]

a=np.abs(a1-a2)




N=100

cmap=np.zeros((N,4))

col1=np.array([255,0,68])

col1=col1/255

for i in range(N):
  alpha=i/N
  cmap[i]=[col1[0],col1[1],col1[2],alpha]

cmap=ListedColormap(cmap)




print("delta_aucs")
print(a)

plt.pcolor(a,cmap=cmap,vmin=0.0,vmax=1.0)
plt.title("difference in Auc")
plt.ylabel("particle")
plt.xlabel("feature")

plt.savefig("images/deltaauc_"+str(fol1)+"_"+str(fol2)+".png",format="png")
plt.savefig("images/deltaauc_"+str(fol1)+"_"+str(fol2)+".pdf",format="pdf")

plt.show()
plt.close() 









