00  raw copy of b1/85
01  mehr batchnorms from 00 in compress
02  auc by epoch from 00
03  abs compare from 00
04  even more batchnorms than 01 (this time in decompress)
05  try some simple skaling from 00
06  more skaling from 05
07  try 06 on gpu (sadly acciendentaly ran xclean in training once)
08  more compressing version of 07
09  like 07, but less compression
10  run 07 on less data again
11  keep 10 running
12  copy of 5 to make const graphs work
13  copy of 12, removing angular information
14  b1/85 on top
15  copy of 13, with learnable degraphs
16  like 13, but learning the initial graph only on angular information
17  15 on param=4
18  17 but rounded graphs
19  like 16, but on top
20  like 05, first one to test compression size (tiny)
21  like 05, second one to test compression size (small)
22  like 05, third one to test compression size (big)
23  like 05, fourth one to test compression size (huge)
24  debug mean recs
25  like 20, but even more compressing
26  85 nosort
27  like 05 but more complicated
28  like 27 but non initialised
29  write in compressor graph actors (wow auc, best yet)
30  keep writing of 29, here mostly decompression
31  rerun 29, but without stopping
32  rerun 31, since worse results than 29 (auc even better)
33  32 on top
34  copy of 30, to write option 2 parameterlike abstraction
35  keep 30 running
36  keep 34 running
37  copy of 34 to write more elegant createmodel
38  running 32 on bug dataset, to compare to elegant/trivial (37)
39  run magic 29 on 4
40  run magic 29 on 24
41  run magic 29 on 12
42  run magic 29 on 9
43  raw0 on 6
44  raw0 on 10
45  43 super
46  hypertrivial 43
47  46 on 9
48  copy of 45 to write better recqual
49  47 on 4
50  copy of 32 with old recqual
51  copy of 37 now with dual compression
52  51 but with relu activation
53  51 but with sort
54  53 but initialised (also not sorted)
55  54 but relu (54+52)
56  49 on 16
57  49 on 40
58  49 on 100
59  supervised 9
60  like 59, but on 16
61  like 59, but on 40
62  like 59, but on 100
63  34, but on gs=9
64  keep 63 running
65  copy of 55, without special stuff: paramlike 6 on qcd
66  65 on top
67  copy of 48 to write even better recqual
68  keep 51 running
69  51 on top
70  copy of 46, to try sparcity regularizers
71  copy of 29 to write better noise evals
72  29 with a signaleinlage (5%)
73  write copy copy learn learn (disabled 55) (also finally disables BatchStore)
74  keep 72 running (canceled due to disk space)
75  74(72) on top (aka 95%)
76  75 on 50%
77  70 but actually sparse
78  teste init norm on copy of 73 (disabled), here with norm (actually nicht 73)
79  teste init norm on copy of 73 (disabled), here without norm
80  only neigbour nets (copy of 78)
81  fixing 53
82  improve particalauc
83  try downloading ttgan/q/55
84  78 + 83
85  84 by hand
86  85 improve again
87  85 on big
88  86 on big
89  keep 88 running
90  run 88 with relu activation in decompression
91  copy of 90 to make more complex decompression work
92  copy of 91 to run 90 without inits
93  try to understand the reproducebility problem/the jump...has batchstore
94  copy of 32, to try norming by hand a bit
95  92 with 2 learnable graphs
96  copy of 92 to write increasing feature numbers (at least a bit)
97  copy of 92 to try to reproduce 29likes
98  run 97 clusterwise
99  copy of 97 to slowly increase complexity
100 copy of 97 with more complex decoder(paramlike)
101 copy of 97 with higher gs(=12, c=3)
102 100 but graphlike
103 101+100+99
104 103 but graphlike
105 copy of 97, but on the second 6 datapoints
106 rerun 105
107 106(105) on next set (offset=12)
108 107 with offset=18
109 107 with offset=24
110 107 with offset=30
111 107 with offset=36 not yet running
112 97, but with more param/particle (gpre6)
113 112 on top
114 try max distance (gmaxd.py) graph builder
115 copy of 114 (disabled) to try graph graph comparisons
116 run 115 paramlike
117 115 but regularised
118 raw0 on second batch
119 rerun 118
120 118 with offset=8
121 118 with offset=12
122 118 with offset=16
123 118 with offset=20
124 118 with offset=24
125 118 with offset=28
126 118 with offset=32
127 118 with offset=36
128 127 with offset=40
129 127 with offset=44
130 127 with offset=48
131 127 with offset=52
132 127 with offset=56
133 127 with offset=60
134 127 with offset=64
135 127 with offset=68
136 127 with offset=72
137 127 with offset=76, also a bit debug recqual so that it kinda works on low stats
138 97 but trying to reproduce 00
139 138 but even more
140 139 but 2*2 instead of 4
141 140 but on 2*3
142 139 but 6
143 141 but 2*4
144 141 but 3*3
145 142 but lossweigthed by 1/(6+i)
146 145 but lossweigthes by 1/(2+i)
147 146 but pt weigthed (1/pt+0.1)
148 145 but actually the rigth ordering (inverse)
149 146 but actually the rigth ordering (inverse)
150 149 but in steps
151 150 but only focusing on the first 4
152 151 but way more extreme
153 139 but only angular
154 139 but varianz punishing
155 154 fixing 154
156 155 but variance adaptive? nope, does not work for some reason
157 155 less intrusive 155  0.05
158 157 but not focussing on 0
159 158 but even less intrusive  0.1
160 159 more extrem  0.2
161 160 more extrem  0.5
162 159 but optimising the c`s
163 161, but disabled to run pt loss
164 163 but less intrusive
165 164 but inverse
166 164 but per event
167 166 but on gs=6
168 167 on gs=8
169 168 on gs=3
170 168 on gs=5
171 168 on gs=7
172 168 on gs=9
173 167 but squared weigths(since 167 has roots=> power 1)
174 173 but squared again (power 2)
175 174 but more basedivision
176 167 but weigthed by eta
177 167 but weigthed by phi
178 175 but by arbitrary weigths (in steps) [1,1,1,1,0.5,0.5]
179 178 but [1,1,1,0.5,0.5,0.5]
180 179 but [1,1,1,0.25,0.25,0.25]
181 179 but [1,1,1,0.75,0.75,0.75]
182 181 but [1,1,1,0,0,0]
183 181 but [1,1,1,1,1,1]
184 183 but [1,1,0.66,0.66,0.33,0.33]
185 183 but [1,0.85,0.7,0.55,0.4,0.25]
186 182 but [1,1,1,0.1,0.1,0.1]
187 183 but just random
188 183 writing per epoch
189 188 writing zero aucs
190 making denselike from 189
191 190 with *10 lr
192 191 but less complex
193 191 but on gs=50
194 make z actually work
195 194 but on gs=200
196 run 195/zadv on all parts of the particle
197 196 but only on pt
198 copy of 188 to write the growing13 [2]
199 198 on [3]
200 198 on [4]
201 198 on [6]
202 198 on [4,2]
203 198 on [3,3]
204 198 on [4,3]
205 198 on [4,4]
206 198 on [3,3,3]
207 198 on [4,3,3]
208 198 on [4,4,3]
209 198 on [4,4,4]
210 198 on [3,3,3]
