import numpy as np
from ageta import gs as sgs



def getnonzerodata(gs=None):
  if gs==None:gs=sgs
  f=np.load("..//..//toptagdataref//val_fairer.npz")
  x=f["xb"][:,:gs,:]
  y=f["y"][:]
  del f

  nonzero=x[:,-1,0]>0
  rx,ry=[],[]
  for take,ax,ay in zip(nonzero,x,y):
    if not take:continue
    rx.append(ax)
    ry.append(ay)

  rx=np.array(rx)
  ry=np.array(ry)
  return rx,ry



if __name__=="__main__":
  rx,ry=getnonzerodata()
  print(len(rx),len(ry))


  



