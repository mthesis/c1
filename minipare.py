import numpy as np
import matplotlib.pyplot as plt
import sys
import os

meanf="41"
meanf=""


job="debug"
if len(sys.argv)>1:job=sys.argv[1]


folders=[]
labels=[]

title=""
lowpow=2.5
lowpow2=4

if job=="cs16":
  folders=["25","20","21","05","22","23"]
  labels=[3,6,10,12,16,24]
  title=None
  lowpow=2.5

if job=="cs64":
  folders=["06","07","08","09","10","11"]
  labels=["20 cpu","20","8","50","20 less data","20 less data, keep"]
  title=None
  lowpow=2.5

if job=="scale":
  folders=["00","05","09"]
  labels=["4","16","64"]
  title=None
  lowpow=2.5

if job=="cgraph16":
  folders=["12","13","15","16","17","18"]
  labels=["simple","no angles","learn degraph","only angle graph","learn degraph param=4","rounding degraph param=4"]
  title=None
  lowpow=2.5

if job=="dtop85":
  folders=["00","14"]
  labels=["qcd","top"]
  title=None
  lowpow=2.5

if job=="dtop16":
  folders=["16","19"]
  labels=["qcd","top"]
  title=None
  lowpow=2.5

if job=="scale29":
  folders=["39","29","42","41","46","47","49"]#,"40"]
  labels=[4,6,9,12,-1,-2,-3]#,24]
  title="scale29"
  lowpow=2.5

if job=="scale0":
  folders=["00","43","44","22"]
  labels=[4,6,10,16]
  title="scale0"
  lowpow=2.5

if job=="debug":
  folders=["48","50"]
  labels=["new","old"]
  title="debug"
  lowpow=2.5


if len(folders)!=len(labels):
  labels=folders



oldroc=True
brandnewroc=True

if len(sys.argv)>2:
  oldroc=bool(int(sys.argv[2]))




def int(x,y):
  x,y=(list(t) for t in zip(*sorted(zip(x,y))))
  ret=0.0
  for i in range(1,len(x)):
    ret+=((y[i]+y[i-1])*(x[i]-x[i-1]))/2
  return ret

fs=9
fig,(ax2,ax1)=plt.subplots(2,figsize=(fs,fs*(3/4)))

if len(meanf)>0:
  fm=np.load(meanf+"/meanroc.npz")
  meanauc=int(1-fm["fpr"],1-fm["tpr"])
  ax2.axhline(meanauc,alpha=0.5,color="orange")


for folder,label in zip(folders,labels):
  f=np.load(folder+"/roc.npz")
  if "nw" in f.files:
    fpr=f["fpr"]#1- because of invert
    tpr=f["tpr"]
    print("called nw once")
  else:
    fpr=f["tpr"]
    tpr=f["fpr"]

    


    print("called ow once")
  auc=int(fpr,tpr)
  
  ax1.plot(fpr,tpr,label=str(label)+": "+str(round(auc,4)))

  if not(job=="best" and auc<0.5):ax2.plot(label,auc,"o")

  print(label,":",auc)

if True:
  ax1.set_xlabel("fpr")
  ax1.set_ylabel("tpr")
  ax1.legend(loc=4)

ax2.set_ylabel("auc")

fig.suptitle(title)


#plt.savefig("images/"+job+".png",format="png")
#plt.savefig("images/"+job+".pdf",format="pdf")




plt.show()










