#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=30G
#SBATCH --job-name=run22
#SBATCH --output=logs/output.%J.txt

#SBATCH --time 180





module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /home/sk656163/m/c1/22/

python3 main.py
./stdana.sh
