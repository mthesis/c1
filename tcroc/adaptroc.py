import numpy as np
import matplotlib.pyplot as plt
import sys

from cauc import caucd,calcd


files=["00","119","120","121","122"]

losses=[0.002801,0.004694,0.008749,0.013044,0.013491]


files+=["123","124","125","126","127","128","129","130","131","132","133","134","135","136","137"]

losses+=[0.017298,0.017688,0.017276,0.015102,0.013745,0.015691,0.014040,0.013376,0.010487,0.009113]

#cut=3
#losses=losses[:-cut]


if len(files)>len(losses):files=files[:len(losses)]

scalepower=3.0


ds=[]

nam="triv"
if len(sys.argv)>1:nam=sys.argv[1]

if len(sys.argv)>2:scalepower=float(sys.argv[2])

def prep(q):
  if nam=="triv":return q
  if nam=="ang":return q[:,:,1:3]
  if nam=="pt":return np.reshape(q[:,:,-1],(int(q.shape[0]),int(q.shape[1]),1)) 

for i,(fil,los) in enumerate(zip(files,losses)):
  f=np.load("data/"+fil+".npz")
  d=np.sqrt(np.mean((prep(f["p"])-prep(f["c"]))**2,axis=(1,2)))
  #dgeneral=np.sqrt(np.mean(((f["p"])-(f["c"]))**2,axis=(1,2)))
  y=f["y"]
  d=np.reshape(d,(int(d.shape[0]),1))
  ds.append(d)
  #losses[i]=np.mean(d[np.where(y<0.5)]**1)
  #losses[i]=np.mean(dgeneral[np.where(y<0.5)]**1)

losses=np.array(losses)

weig=1/losses**(scalepower/2)


weig=np.reshape(weig,(len(weig),1))

gss=[]
aucs=[]

for i in range(len(losses)):
  ad=np.concatenate(ds[:i+1],axis=-1)
  aweig=weig[:i+1]
  #print(ad.shape,aweig.shape)
  #exit()
  ad=np.dot(ad,aweig)
  auc=caucd(d=ad,y=y)["auc"]
  aucs.append(auc)
  gss.append((i+1)*4)
  print(gss[-1],":",aucs[-1])

np.savez_compressed("out/"+nam+str(scalepower),auc=aucs,gs=gss)




