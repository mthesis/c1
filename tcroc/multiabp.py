import numpy as np
import matplotlib.pyplot as plt
import sys

from cauc import caucd,calcd




files4=["00","c00_119","cc00_119_120","ccc00_119_120_121","cccc00_119_120_121_122"]
names4=[4,8,12,16,20]
files6=["97","c97_105","cc97_105_107","ccc97_105_107_108"]
names6=[6,12,18,24]


def pj(j,alpha=1.0):
  if alpha==1.0:
    print("auc",j["auc"],"e30",j["e30"])
  else:
    print(alpha,":","auc",j["auc"],"e30",j["e30"])

def getj(q):
  f=np.load("data/"+q+".npz")
  y=f["y"]
  if ("justd" in f.files):
    d=f["d"]
  else:
    p=f["p"]
    c=f["c"]
    d=calcd(p=p,c=c)
  j=caucd(d=d,y=y)
  return j
def plotq(q,nam,maxauc=0.0):
  j=getj(q)
  fpr=j["fpr"]
  tpr=j["tpr"]
  plt.plot(tpr,1/(fpr+0.00001),label=str(nam))
  plt.xlabel("tpr")
  plt.ylabel("1/fpr")
  auc=j["auc"]
  if auc>maxauc:maxauc=auc
  return maxauc,auc 


aucs4=[]
aucs6=[]
for fil in files4:aucs4.append(getj(fil)["auc"])
for fil in files6:aucs6.append(getj(fil)["auc"])


c1="blue"
c2="orange"


plt.plot(names4,aucs4,marker="o",label="4",color=c1)
plt.plot(names6,aucs6,marker="o",label="6",color=c2)

plt.axhline(getj("closs4")["auc"],color=c1)
plt.axhline(getj("closs6")["auc"],color=c2)

plt.title("auc by particles")
plt.legend()
plt.savefig("imgs/abp.png",format="png")
plt.savefig("imgs/abp.pdf",format="pdf")
plt.show()





