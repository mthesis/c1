import numpy as np
import matplotlib.pyplot as plt
import sys

from cauc import caucd,calcd


q="ccc00_119_120_121"
if len(sys.argv)>1:q=sys.argv[1]

def pj(j,alpha=1.0):
  if alpha==1.0:
    print("auc",j["auc"],"e30",j["e30"])
  else:
    print(alpha,":","auc",j["auc"],"e30",j["e30"])

f=np.load("data/"+q+".npz")


y=f["y"]


if ("justd" in f.files):
  d=f["d"]
else:
  p=f["p"]
  c=f["c"]
  d=calcd(p=p,c=c)






j=caucd(d=d,y=y)

pj(j)

fpr=j["fpr"]
tpr=j["tpr"]

plt.plot(tpr,1/(fpr+0.00001))
plt.xlabel("tpr")
plt.ylabel("1/fpr")
plt.yscale("log",nonposy="clip")
plt.ylim([1,1000])


plt.title(str(round(j["auc"],6)))

plt.show()


