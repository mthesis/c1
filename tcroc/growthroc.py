import numpy as np
import matplotlib.pyplot as plt
import sys

from cauc import caucd,calcd




job="4"
if len(sys.argv)>1:job=sys.argv[1]


if job=="4":
  files=["00","c00_119","cc00_119_120","ccc00_119_120_121","cccc00_119_120_121_122"]
  names=[4,8,12,16,20]
if job=="6":
  files=["97","c97_105","cc97_105_107","ccc97_105_107_108"]
  names=[6,12,18,24]


def pj(j,alpha=1.0):
  if alpha==1.0:
    print("auc",j["auc"],"e30",j["e30"])
  else:
    print(alpha,":","auc",j["auc"],"e30",j["e30"])

def getj(q):
  f=np.load("data/"+q+".npz")
  y=f["y"]
  if ("justd" in f.files):
    d=f["d"]
  else:
    p=f["p"]
    c=f["c"]
    d=calcd(p=p,c=c)
  j=caucd(d=d,y=y)
  return j
def plotq(q,nam,maxauc=0.0):
  j=getj(q)
  fpr=j["fpr"]
  tpr=j["tpr"]
  plt.plot(tpr,1/(fpr+0.00001),label=str(nam))
  plt.xlabel("tpr")
  plt.ylabel("1/fpr")
  auc=j["auc"]
  if auc>maxauc:maxauc=auc
  return maxauc,auc 

maxauc=0.0
aucs=[]
for i in range(len(files)):
  maxauc,acauc=plotq(files[i],names[i],maxauc)
  aucs.append(acauc)

plt.legend()
plt.yscale("log",nonposy="clip")
plt.ylim([1,1000])
plt.title(str(round(maxauc,6)))
plt.savefig("imgs/roc"+job+".png",format="png")
plt.savefig("imgs/roc"+job+".pdf",format="pdf")

plt.close()



plt.plot(names,aucs,marker="o")
plt.title("auc by particles for type " + job)
plt.savefig("imgs/abp"+job+".png",format="png")
plt.savefig("imgs/abp"+job+".pdf",format="pdf")
plt.show()





