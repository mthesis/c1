import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd,calcd

#type 4x
q1="00"
q2="119"

q1="c00_119"
q2="120"

q1="cc00_119_120"
q2="121"

q1="ccc00_119_120_121"
q2="122"


#type 6x
q1="97"
q2="105"

q1="c97_105"
q2="107"

q1="cc97_105_107"
q2="108"


def pj(j,alpha=1.0):
  if alpha==1.0:
    print("auc",j["auc"],"e30",j["e30"])
  else:
    print(alpha,":","auc",j["auc"],"e30",j["e30"])

def combalpha(d1,d2,alpha=1.0):
  return (d1+d2*alpha)#/(1+alpha)
def calcbyalphasum(d1,d2,y1,y2,alpha=1.0):
  assert((y1==y2).all())
  d=combalpha(d1,d2,alpha=alpha)
  j=caucd(d=d,y=y1)
  return j

f1=np.load("data/"+q1+".npz")
f2=np.load("data/"+q2+".npz")

#print(f1.files,f2.files)
#print("justd" in f1.files,"justd" in f2.files)

#exit()


y1=f1["y"]
y2=f2["y"]


if ("justd" in f1.files):
  d1=f1["d"]
else:
  p1=f1["p"]
  c1=f1["c"]
  d1=calcd(p=p1,c=c1)
if ("justd" in f2.files):
  d2=f2["d"]
else:
  p2=f2["p"]
  c2=f2["c"]
  d2=calcd(p=p2,c=c2)






j1=caucd(d=d1,y=y1)
j2=caucd(d=d2,y=y2)

pj(j1)
pj(j2)


j=calcbyalphasum(d1,d2,y1,y2,alpha=1.0)
pj(j)




fcon=1.05
fnum=50
fnul=0.10

alphas=[]
aucs=[]
js=[]

for i in range(-fnum,fnum+1):
  alpha=fnul*(fcon**i)
  alphas.append(alpha)
  acj=calcbyalphasum(d1,d2,y1,y2,alpha=alpha)
  pj(j=acj,alpha=alpha)  
  aucs.append(acj["auc"])
  js.append(acj)


bi=np.argmax(aucs)
bj=js[bi]
ba=alphas[bi]

np.savez_compressed("data/c"+q1+"_"+q2,justd=[1],alpha=[ba],y=y1,d=combalpha(d1=d1,d2=d2,alpha=ba))


pj(bj)



plt.plot(alphas,aucs)
plt.xlabel("alpha")
plt.ylabel("auc")

plt.show()




