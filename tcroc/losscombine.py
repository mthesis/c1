import numpy as np
import matplotlib.pyplot as plt
import sys

from cauc import caucd,calcd

typ=4
if len(sys.argv)>1:typ=int(sys.argv[1])

if typ==4:
  #type 4x
  q1="00"
  q2="119"
  q3="120"
  q4="121"
  q5="122"

  l1=0.002801
  l2=0.004694
  l3=0.008749
  l4=0.013044
  l5=0.013491

  q=[q1,q2,q3,q4,q5]
  l=[l1,l2,l3,l4,l5]
elif typ==6:
  #type 6x
  q1="97"
  q2="105"
  q3="107"
  q4="108"

  l1=0.003616
  l2=0.009010
  l3=0.015921
  l4=0.019346

  q=[q1,q2,q3,q4]
  l=[l1,l2,l3,l4]




l=np.array(l)


def pj(j,alpha=1.0):
  if alpha==1.0:
    print("auc",j["auc"],"e30",j["e30"])
  else:
    print(alpha,":","auc",j["auc"],"e30",j["e30"])

def combalpha(d1,d2,alpha=1.0):
  return (d1+d2*alpha)#/(1+alpha)
def combmalpha(d,alpha):
  ret=d[0]*alpha[0]
  for i in range(1,len(alpha)):
    ret+=d[i]*alpha[i]
  return ret
def calcbyalphasum(d1,d2,y1,y2,alpha=1.0):
  assert((y1==y2).all())
  d=combalpha(d1,d2,alpha=alpha)
  j=caucd(d=d,y=y1)
  return j

def loadx(q):
  f=np.load("data/"+q+".npz")
  y=f["y"]
  if ("justd" in f.files):
    d=f["d"]
  else:
    p=f["p"]
    c=f["c"]
    d=calcd(p=p,c=c)
  return y,d


ys=[]
ds=[]
for aq in q:
  ay,ad=loadx(aq)
  ys.append(ay)
  ds.append(ad)

alpha=l[0]/l

alpha=alpha**2

#print(alpha)
#exit()

qd=combmalpha(ds,alpha)
j=caucd(d=qd,y=ys[0])

pj(j)

np.savez_compressed("data/closs"+str(typ),justd=[1],alpha=["losslike"],y=ys[0],d=qd)
