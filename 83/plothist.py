import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open("history.csv","r") as ff:
  c=csv.reader(ff,delimiter=",")
  jumped=False
  q=[]
  qn=[]
  epoch=[]
  for row in c:
    if not jumped:
      for e in row:
        q.append([])
        qn.append(e)
      jumped=True
      continue

    for i in range(len(row)):
      q[i].append(float(row[i]))


for i in range(1,len(q)):
  if qn[i]=="lr":
    q[i]=np.log(q[i])
    q[i]-=np.mean(q[i])
    q[i]/=np.std(q[i])*4
    q[i]+=0.75
  plt.plot(q[0],q[i],label=qn[i])

plt.xlabel(qn[0])
plt.legend()


plt.savefig("history.png",format="png")
plt.savefig("history.pdf",format="pdf")

plt.show()

