import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model


from icreatemodel import *



batch_size=200
max_epochs=10000
verbose=2
patience=30
learning_rate=0.003


gs=30
n=1000#000000
vn=1000#000000



f=np.load("..//..//..//toptagdataref//train_short.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f
vf=np.load("..//..//..//toptagdataref//val_short.npz")
vx=vf["xb"][:vn,:gs,:]
vy=vf["y"][:vn]
del vf

y=keras.utils.to_categorical(y,2)
vy=keras.utils.to_categorical(vy,2)








model=createmodel(gs=gs,n=n,vn=vn,batch_size=batch_size)

#from gpre2 import *
#model=Sequential([gpre2(gs=gs)])

#x=np.zeros_like(x)

#px=(model.predict(x))
#print(px.shape)
#print(px[0])
#exit()

model.summary()
print("Input_shape:",model.input_shape)
#print("Output_shape:",model.compute_output_shape(model.input_shape))

#exit()


model.compile(RMSprop(lr=learning_rate),loss="categorical_crossentropy",metrics=['accuracy'])




i=0


p1=model.predict(x,batch_size=batch_size)
print(x[i])
print(p1[i])
print(x.shape,"=>",p1.shape)
print(np.sum(p1[0],axis=-1))

#print(np.max(np.sum(p1,axis=-1)))
#sum=np.sum(p1,axis=-1)
#no0sum=np.where(sum>0,sum,np.zeros_like(sum)+100)
#print(np.min(no0sum))


#exit()
#print(np.max(p1))
#print(np.sum(p1[i][0]),p1[i][0])
#print(np.sum(p1))


np.savez_compressed("graphs",q=p1)















