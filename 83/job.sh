#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=80G
#SBATCH --job-name=run51
#SBATCH --output=logs/output.%J.txt
#SBATCH --gres=gpu:1
#SBATCH --time 240





module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /home/sk656163/m/ttgan/q/51/

python3 main.py
