import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model


from createmodel import *
from AdamW import *


batch_size=400
max_epochs=10000
verbose=2
patience=30
learning_rate=0.001


gs=100
n=1000000000
vn=1000000000



f=np.load("..//..//..//toptagdataref//train.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f
vf=np.load("..//..//..//toptagdataref//val.npz")
vx=vf["xb"][:vn,:gs,:]
vy=vf["y"][:vn]
del vf

y=keras.utils.to_categorical(y,2)
vy=keras.utils.to_categorical(vy,2)








model=createmodel(gs=gs,n=n,vn=vn,batch_size=batch_size)


model.summary()
print("Input_shape:",model.input_shape)
#print("Output_shape:",model.compute_output_shape(model.input_shape))

#opt=RMSprop(lr=learning_rate)
opt=Adam(lr=learning_rate)
#opt=AdamW(lr=learning_rate,weight_decay=0.0001,batch_size=batch_size,samples_per_epoch=int(len(x)/batch_size),name="AdamW_01")

model.compile(opt,loss="categorical_crossentropy",metrics=['accuracy'])





fit = model.fit(
    x, y,                          # training data
    batch_size=batch_size,         # size of each batch
    epochs=max_epochs,             # number of training epochs
    verbose=verbose,               # verbosity of shell output (0: none, 1: high, 2: low)
    validation_data=(vx, vy),      # validation data
    callbacks=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=patience),
               keras.callbacks.ModelCheckpoint("modelb.h5", monitor='val_loss', verbose=verbose, save_best_only=True),
               keras.callbacks.ModelCheckpoint("modelwb.h5", monitor='val_loss', verbose=verbose, save_best_only=True,save_weights_only=True),
               keras.callbacks.ModelCheckpoint("modela.h5", monitor='val_acc', verbose=verbose, save_best_only=True, mode='auto'),
               keras.callbacks.ModelCheckpoint("modelwa.h5", monitor='val_acc', verbose=verbose, save_best_only=True, mode='auto',save_weights_only=True),
               
               keras.callbacks.ModelCheckpoint("models/model{epoch:04d}.h5",verbose=0,save_best_only=False,period=1,save_weights_only=False),         
               keras.callbacks.ModelCheckpoint("models/weights{epoch:04d}.h5",verbose=0,save_best_only=False,period=1,save_weights_only=True),               

#               keras.callbacks.LearningRateScheduler(schedule=schedule,verbose=1),

               keras.callbacks.TerminateOnNaN(),
               keras.callbacks.CSVLogger("history.csv")
    ])     



print("reached model quality of ",np.max(fit.history["val_acc"]),np.min(fit.history["val_loss"]))
np.savez_compressed("result",val_acc=np.max(fit.history["val_acc"]),val_loss=np.min(fit.history["val_loss"]))



model_json=model.to_json()
with open("model.json","w") as f:
  f.write(model_json)



