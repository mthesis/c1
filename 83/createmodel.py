import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Conv2D,BatchNormalization,Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gpre1 import *
from gpre2 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from dedge import *
from dexpand import  *
from dnarrow import *
from dtorb import *
from dreflag import *
from dtcreate import *
from dtdestroy import *
from gaddzeros import *

objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gpre1":gpre1,"gpre2":gpre2,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"dedge":dedge,"dexpand":dexpand,"dnarrow":dnarrow,"dtorb":dtorb,"dreflag":dreflag,"dtcreate":dtcreate,"dtdestroy":dtdestroy,"gaddzeros":gaddzeros}



def schedule(epoch,lr):
  a=3E-4
  b=3E-5
  d=8
  d1=d
  d2=d
  c=5E-7
  tc=4
  if epoch==0:return a#kinds useless actually, but why not
  if epoch<d1:
    return a+(b-a)*(epoch/(d1-1))
  if epoch<d1+d2:
    return b+(a-b)*((epoch-d1)/(d2-1))
  if epoch>=d1+d2+tc:return c
  return a+(c-a)*((epoch-d1-d2)/(tc-1))
  

def EdgeConv2(inp,gs,param0,param1,k=16,paramm1=8):
  mat,feat=gtopk(gs=gs,k=k,param=param0,free=param1-param0,flag=paramm1-1)([inp])
  #feat2=gltrivmlp(gs=gs,param=param1,k=k,keepconst=paramm1,iterations=1,alinearity=[],i1=param1,i2=param1)([mat,feat])
  feat2=glmlp(gs=gs,param=param1,k=k,keepconst=paramm1,iterations=1,alinearity=[0.0,"inf"],i1=param1,i2=param1)([mat,feat])
  #mat2=gadd1(gs=gs)([mat])
  #feat2=gltk(gs=gs,param=param1,keepconst=paramm1,iterations=1,alinearity=[0.0,"inf"])([mat2,feat])
  return Concatenate()([feat,feat2]),param1*2

def myconv(inp,gs,k,param):
  feat=dtorb(gs=gs,k=k,param=param,flag=0)([inp])
  return Concatenate(axis=-1)([inp,feat]),param*2


def subedge(inp,param): 
  feat1=Conv2D(param,(1,1),use_bias=False)(inp)
  feat2=BatchNormalization()(feat1)
  feat3=Activation("relu")(feat2)
  return feat3

def myconv2(inp,gs,k,param):
  feat0=dtcreate(gs=gs,k=k,param=param,flag=0)([inp])
  feat1=subedge(feat0,param)
  feat2=subedge(feat1,param)
  feat3=subedge(feat2,param)
  qfeat=dtdestroy(gs=gs,k=k,param=param)([feat3])
  outp=Activation("relu")(qfeat)
  return Concatenate(axis=-1)([inp,outp]),param*2


def createmodel(gs,n,batch_size=200,**kwargs):


  param0=8
  param1=64  

  k=16
  param=param1

  inputs=Input(shape=(gs,4,))
  
  feat0=gpre2(gs=gs)(inputs)
  


  #mat1,feat1=gtopk(gs=gs,k=16,param=param0,free=param1-param0,flag=7)([feat0])
  #feat2=dexpand(gs=gs,param=param1)([feat1])
  feat1=dreflag(gs=gs,param=param1,flagI=7,flagO=0)([feat0])
  feat2=gaddzeros(gs=gs,param0=param0,param1=param1)([feat1])
  feat3,param=myconv2(feat2,gs=gs,k=k,param=param)
  feat4,param=myconv2(feat3,gs=gs,k=k,param=param)
  feat5,param=myconv2(feat4,gs=gs,k=k,param=param)


  pooled=gpool(gs=gs,param=param,mode="mean")([feat5])

  flatpool=Flatten()(pooled)
  
  d1=Dense(100,activation="relu")(flatpool)
  
  dd=Dropout(0.1)(d1)

  outputs=Dense(2,activation="softmax")(dd)

  model=Model(inputs=inputs,outputs=outputs)




  return model















