import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





batch_size=100
epochs=1000
verbose=2
lr=0.001

def phieta_init(shape,dtype=None):
  rel=np.zeros(shape)
  if len(rel)>=4:
    rel[4,0]=1.0
  if len(rel)>=5:
    rel[5,0]=1.0
  return K.constant(rel,dtype=dtype)


class gaddzeros(Layer):
  def __init__(self,gs=30,param0=8,param1=64,**kwargs):
    
    #print("initilized a new gaddzeros with gs=",gs)
    self.gs=gs
    self.param0=param0
    self.param1=param1

    super(gaddzeros,self).__init__(**kwargs)

  def get_config(self):
    mi={"gs":self.gs,"param0":self.param0,"param1":self.param1}
    th=super(gaddzeros,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return gaddzeros(**config)
  
  def build(self, input_shape):


    self.built=True



  def call(self,x):
    x=x[0]

    #print("!x",x.shape)

    gs=self.gs
    zeros=self.param1-self.param0

    zero1=K.zeros_like(x[:,:,0])
    zero1=K.reshape(zero1,(-1,gs,1))
    #print("!",zero1.shape)
    zerolis=[]
    for i in range(zeros):
      zerolis.append(zero1)
    zeros=K.concatenate(zerolis,axis=-1)
    #print(zeros.shape)

    return K.concatenate((x,zeros),axis=-1)



    



    
  def compute_output_shape(self,input_shape):
    shape=list(input_shape[0])
    #print("inputting",shape,"gs=",self.gs,"sp=",self.param)
    assert len(shape)==3
    assert shape[-1]==self.param0
    assert shape[-2]==self.gs

    shape[-1]=self.param1

    return tuple(shape)













