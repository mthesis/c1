import tensorflow.keras as keras
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras.initializers as ini
import numpy as np




def flipinit(shape,dtype=None):
  assert len(shape)==2
  rel=np.zeros(shape)
  for i in range(np.min(shape)):
    rel[i,shape[-1]-i-1]=1.0
  return K.constant(rel,dtype=dtype)


def testinit(shape,dtype=None):
  return ini.Orthogonal()(shape,dtype=dtype)


def otherIdent(shape,dtype=None):
  assert len(shape)==2
  return K.reverse(ini.Identity()(shape,dtype=dtype),axes=(-1,-2))





