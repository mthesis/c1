import numpy as np
import matplotlib.pyplot as plt
import sys
import os

meanf="41"



job="best"
sjob=-1
if len(sys.argv)>1:sjob=int(sys.argv[1])


folders=[]
labels=[]
inv=[]

title=""
lowpow=2.5
alpha=1.0


if job=="best":
  #folders=["12","27","31","60","74","45/super"]
  #labels=["cs=1","fixed loss","ter","highbatchnoninit","fixed","supervised"]
  folders=["43/mean","../rroc/QCDorWhat/worst_img.npz","../rroc/QCDorWhat/best_img.npz","../rroc/QCDorWhat/Lola.npz","32","45/super"]
  labels=["mean","QoW image(6)","QoW image(32)","QoW Lola","gae","supervised"]
  invert=[True,False,False,False,True,True]
  colors=["red","#000000","#333333","#666666","#33cc33","#006600"]
  title=None
  lowpow=2.5
  lowpow2=4.0
  alpha=0.8




folders=np.array(folders)
labels=np.array(labels)
invert=np.array(invert)
colors=np.array(colors)



dex=[0,1,2,3,4,5]


if sjob==0:
  dex=[0]
if sjob==1:
  dex=[0,1,2,3]
if sjob==2:
  dex=[0,1,2,3,5]
if sjob==3:
  dex=[0,1,2,3,4,5]



folders=folders[dex]
labels=labels[dex]
invert=invert[dex]
colors=colors[dex]




if len(folders)!=len(labels):
  labels=folders



oldroc=False
brandnewroc=True

if len(sys.argv)>2:
  oldroc=bool(int(sys.argv[2]))




def int(x,y):
  x,y=(list(t) for t in zip(*sorted(zip(x,y))))
  ret=0.0
  for i in range(1,len(x)):
    ret+=((y[i]+y[i-1])*(x[i]-x[i-1]))/2
  return ret

fs=8
fig,ax1=plt.subplots(1,figsize=(fs,fs*(3/4)))


#fm=np.load(meanf+"/meanroc.npz")
#meanauc=int(1-fm["fpr"],1-fm["tpr"])


for folder,label,shallinv,col in zip(folders,labels,invert,colors):
  if os.path.isfile(folder+"/roc.npz"):
    f=np.load(folder+"/roc.npz")
    if "nw" in f.files:
      shallinv=False
    else:
      col="red"
  elif os.path.isfile(folder+"roc.npz"):
    f=np.load(folder+"roc.npz")
  else:
    f=np.load(folder)

  

  if shallinv:
    fpr=f["tpr"]#1- because of invert
    tpr=f["fpr"]
  else:
    fpr=f["fpr"]
    tpr=f["tpr"]
  auc=int(fpr,tpr)


  lab=None
  if label!=None:
    lab=str(label)
    if auc>0.5:lab=lab+": "+str(round(auc,4))
  
  if oldroc:
    ax1.plot(fpr,tpr,label=lab,color=col,alpha=alpha)
  elif brandnewroc:
    ax1.plot(tpr,1/(fpr+0.00001),label=lab,color=col,alpha=alpha)
  else:
    ax1.plot(tpr,fpr,label=lab,color=col,alpha=alpha)


  print(label,":",auc)

if oldroc:
  ax1.set_xlabel("fpr")
  ax1.set_ylabel("tpr")
  ax1.legend(loc=4)
elif brandnewroc:
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlim([-0.05,1])
  ax1.set_ylim((1,10**(lowpow2)))
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlabel("true positive rate")
  ax1.set_ylabel("1/false positive rate")
  ax1.legend(loc=1)
else:
  ax1.set_xlim([0,1])
  ax1.set_ylim((10**(-lowpow),1))
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlabel("false positive rate")
  ax1.set_ylabel("true positive rate")
  ax1.legend(loc=4)


fig.suptitle(title)


plt.savefig("images/recc"+str(sjob)+".png",format="png")
plt.savefig("images/recc"+str(sjob)+".pdf",format="pdf")




plt.show()










