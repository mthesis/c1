import numpy as np
import matplotlib.pyplot as plt




folders=["00","119"]



tprs=[]
fprs=[]



for folder in folders:
  f=np.load(folder+"/roc.npz")
  
  tprs.append(f["tpr"])
  fprs.append(f["fpr"])








def combine(tpra,fpra,tprb,fprb):
  fprs,tprs=[],[]
  for ka in np.arange(0,1,0.1):
    for kb in np.arange(0,1,0.1):
      ta=fpra>ka
      tb=fprb>kb
      f11=np.where(np.logical_and(ta,tb),fpra*fprb,0)
      f10=np.where(np.logical_and(ta,np.logical_not(tb)),fpra,0)
      f01=np.where(np.logical_and(tb,np.logical_not(ta)),fprb,0)
      f00=np.where(np.logical_and(np.logical_not(ta),np.logical_not(tb)),fpra+fprb-fpra*fprb,0)
      t11=np.where(np.logical_and(ta,tb),tpra*tprb,0)
      t10=np.where(np.logical_and(ta,np.logical_not(tb)),tpra,0)
      t01=np.where(np.logical_and(tb,np.logical_not(ta)),tprb,0)
      t00=np.where(np.logical_and(np.logical_not(ta),np.logical_not(tb)),tpra+tprb-tpra*tprb,0)

      fpr=f11+f00+f01+f10
      tpr=t11+t00+t01+t10
      

      fprs.append(fpr)
      tprs.append(tpr)
  return np.concatenate(fprs),np.concatenate(tprs)



cfpr,ctpr=combine(tprs[0],fprs[0],tprs[1],fprs[1])

#print(len(cfpr),len(ctpr))


#exit()


plt.plot(cfpr,ctpr,"o",alpha=0.3)



for i in range(len(tprs)):
  plt.plot(fprs[i],tprs[i],label=folders[i])




#plt.legend()
plt.show()


