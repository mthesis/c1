import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *


from createmodel import *




def createsupervised(gs,**kwargs):
  
  alin=[-1.0,1.0]
  k=2

  np=10

  comp=[8,4,2]


  inputs=Input(shape=(gs,4,))

  feat1=inputs

  feat1=gpre5(gs=gs)(feat1) 

  s=state(gs=gs,param=4)

  feat1=BatchNormalization(axis=-1)(feat1)

  feat1,s=actions(feat1,s,alin=alin,k=k)

  feat1,s=compress(feat1,s,np,3) 
  feat1,s=actions(feat1,s,alin=alin,k=k)

  feat1,s=compress(feat1,s,np,3) 

  feat1=ggoparam(gs=s.gs,param=s.param)([feat1])

  feat1=multidense(feat1,comp,activation="relu")

  feat1=Dense(2,activation="softmax")(feat1)
  feat1=Activation("softmax")(feat1)


  return Model(inputs=inputs,outputs=feat1)









