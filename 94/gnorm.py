import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class gnorm(Layer):
  def __init__(self,ishape=(20,4),norm=[1.0,1.0,1.0,1.0],delta=[0.0,0.0,0.0,0.0],**kwargs):
    assert ishape[-1]==len(norm)
    assert len(norm)==len(delta)
    self.ishape=ishape
    self.norm=norm
    self.delta=delta
    super(gnorm,self).__init__(**kwargs)

  def build(self, input_shape):

    self.built=True


  def call(self,x):
    x=x[0]
 
    #print("x",x.shape)
    #print("norm",self.norm.shape)

    xn=(x-self.delta)*self.norm
    #print("xn",xn.shape)

    #exit()
   
    return xn 

    

    
  def compute_output_shape(self,input_shape):
    input_shape=input_shape[0]
    assert len(input_shape)==len(self.ishape)+1#+1 batch dimension
    for i in range(1,len(input_shape)):
      assert input_shape[i]==self.ishape[i-1]
    return tuple(input_shape)    

  def get_config(self):
    mi={"ishape":self.ishape,"norm":self.norm,"delta":self.delta}
    th=super(gnorm,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return gnorm(**config)













