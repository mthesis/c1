
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.regularizers import Regularizer
import tensorflow.keras.backend as K



class sparse_reg(Regularizer):#assumes sigmoid activation (aka nonactivated=0)

  def __init__(self, p=0.01,beta=3.0,state0=0.0):
    self.p = K.cast_to_floatx(p)
    self.beta=K.cast_to_floatx(beta)
    self.state0=K.cast_to_floatx(state0)

  def __call__(self, x):
    print("called sparsereg on",x)
    p_hat=K.mean(K.abs(x-self.state0))
    print("calc p_hat",p_hat)
    #exit()
    KLD=(self.p-p_hat)**2
    #KLD=self.p*(K.log(K.abs(self.p/p_hat))) + (1-self.p)*(K.log(K.abs((1-self.p)/(1-p_hat))))
    return self.beta*K.sum(KLD)


  def get_config(self):
    return {'p': float(self.p),'beta':float(self.beta),'state0':float(self.state0)}


















