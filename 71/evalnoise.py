import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd
from numpy.random import normal

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv2load import *
from ageta import *


f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f


sp=x.shape
s=sp[1:]


def rndevent():
  ret=np.zeros(s)
  for i in range(s[0]):
    for j in range(s[1]):
      q=np.random.randint(sp[0])
      ret[i,j]=x[q,i,j]
  return ret


def rndevents(n=1000):
  ret=[]
  for i in range(n):
    ret.append(rndevent())
  return np.array(ret)


print("generating data")


x=rndevents(x.shape[0])
y=np.zeros_like(y)+3


print("generated data")

#y=keras.utils.to_categorical(y,2)




#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

mis=["b"]#,"a"]

if len(sys.argv)>1:
  cut=int(sys.argv[1])



  mis=[["b","a"][cut]]

for mi in mis:
  model=a2load(mi,gs=gs,n=n)
  modelc=getcomp()


  model.summary()





  my=model.predict(x,verbose=1)
  mc=modelc.predict(x,verbose=1)
  np.savez_compressed("evalnoise"+mi,x=x,y=y,p=my,c=mc)




