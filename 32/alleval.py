import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv2load import *
from ageta import *


f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f

#y=keras.utils.to_categorical(y,2)




#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               


startup=1

if len(sys.argv)>1:
  startup=int(sys.argv[1])


def makelenx(x,l=4):
  x=str(x)
  while len(x)<l:x="0"+x
  return x

modelc=getcomp()
mc=modelc.predict(x,verbose=1)
for i in range(startup,1046):
  model=aload("models/weights"+makelenx(i)+".tf",gs=gs,n=n)


  model.summary()





  my=model.predict(x,verbose=1)
  np.savez_compressed("alleval/"+str(i),x=x,y=y,p=my,c=mc)
  for j in range(100):print("did",i)



