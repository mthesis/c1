import numpy as np
import csv as csv
from os.path import isfile
import sys



def loss(key="loss",fil="history",pref=""):
  with open(pref+fil+".csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        q[i].append(float(row[i]))
    for i in range(len(qn)):
      if qn[i]==key:
        return q[i]
 
def minloss(pref=""):
  return np.min(loss("loss",pref=pref))
def minvloss(pref=""):
  return np.min(loss("val_loss",pref=pref))
def roc(fil="roc",pref=""):
  #print("loading",pref+fil+".npz")
  if not isfile(pref+fil+".npz"):return 0,0
  f=np.load(pref+fil+".npz")
  return f["auc"],f["e30"]
def arb(pref=""):return roc("data/arbite",pref=pref)
def twoarb(pref=""):return roc("data/twoarbite",pref=pref)
def invarb(pref=""):return roc("data/invarbite",pref=pref)





def main():
  print("loss",minloss())
  print("val_loss",minvloss())
  auc,e30=roc()
  print("auc",auc)
  print("e30",1/(e30+0.0001))

if __name__ == "__main__":main()







