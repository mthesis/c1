import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import isfile

from matplotlib.colors import ListedColormap, LinearSegmentedColormap



from cauc import cauc,caucd,difference as diff


#folders=["97","106","107","108","109","110","111"]#####magic 19 structure
folders=["00","119","120","121","122","123","124"]#####00 structure

#folders=["00"]



if len(sys.argv)>2:
  folders=[]
  for i in range(2,len(sys.argv)):
    folders.append(sys.argv[i])


key=""
if len(sys.argv)>1:
  key=sys.argv[1]

shallnorm="n" in key



fil="evalb"


xx,yy,pp,cc=[],[],[],[]



def trafo(x,y,p,c):
  #lli=np.where(y<0.5)[0]

  #s=np.sqrt(np.mean((p[lli]-c[lli])**2))
  #ss=np.std(np.sqrt(np.mean((p[lli]-c[lli])**2,axis=(1,2))))
  
  #print("s",s,"ss",ss)


  return x,p,c

for folder in folders:

  f=np.load(folder+"/"+fil+".npz")

  x=f["x"]
  y=f["y"]
  p=f["p"]
  c=f["c"]
  x,p,c=trafo(x,y,p,c)
  xx.append(x)
  pp.append(p)
  cc.append(c)

x=np.concatenate(xx,axis=1)
p=np.concatenate(pp,axis=1)
c=np.concatenate(cc,axis=1)


if True:
  d=[]
  pn=int(c.shape[1])
  tn=int(c.shape[1])*int(c.shape[2])
  for i in range(tn-3*pn,tn-pn):
    d.append(i)
  shallsaved=True


def xnorm(p,c,y):
  #shall return 'skalares' d,y,shape
  #return xnorm2(p,c,y)

  shape=p.shape

  if len(p.shape)>2:
    p=np.reshape(p,(p.shape[0],np.prod(p.shape[1:])))
    c=np.reshape(c,(c.shape[0],np.prod(c.shape[1:])))



  ni=int(p.shape[0])
  nq=int(p.shape[1])
  d=np.zeros_like(p)
  for i in range(ni):
    for q in range(nq):
      d[i,q]=diff(p[i,q],c[i,q])
  ind=np.where(y<0.5)[0]
  ad=d[ind,:]
  m=np.mean(ad,axis=0)
  s=np.std(ad,axis=0)
  d-=m
  d/=s

  d=np.sqrt(np.mean(d**2,axis=1))



  #print("d",d.shape,"y",y.shape)
  return d,y,shape

def xnorm2(p,c,y):
  #shall return 'skalares' d,y,shape

  shape=p.shape

  if len(p.shape)>2:
    p=np.reshape(p,(p.shape[0],np.prod(p.shape[1:])))
    c=np.reshape(c,(c.shape[0],np.prod(c.shape[1:])))





  ni=int(p.shape[0])
  nq=int(p.shape[1])

  n=int(nq/4)

  d=np.zeros((ni,n))
  dd=np.zeros((ni,nq))
  for i in range(ni):
    for q in range(n):
      d[i,q]=diff(p[i,4*q:4*(q+1)],c[i,4*q:4*(q+1)])
      for w in range(4):
        dd[i,4*q+w]=d[i,q]
  ind=np.where(y<0.5)[0]
  ad=dd[ind,:]
  m=np.mean(ad,axis=0)
  s=np.std(ad,axis=0)
  d=dd
  d-=m
  d/=s

  d=np.sqrt(np.mean(d**2,axis=1))



  #print("d",d.shape,"y",y.shape)
  return d,y,shape

def partiate(p,c,y,d1l,d1h,d2l,d2h):
  return p[:,d1l:d1h,d2l:d2h],c[:,d1l:d1h,d2l:d2h],y
def partiate2(p,c,y,d):
  ap=np.reshape(p,(p.shape[0],p.shape[1]*p.shape[2]),order="F")
  ac=np.reshape(c,(c.shape[0],c.shape[1]*c.shape[2]),order="F")
  return ap[:,d],ac[:,d],y
def test(p,c,y):
  ac=cauc(p=p,c=c,y=y)
  return ac["auc"],ac["e30"],p.shape,ac
def testd(d,y,shape):

  #print("d",d.shape,type(d),"y",y.shape,type(y))
  #exit()

  ac=caucd(d=d,y=y)
  #print("done",ac["auc"])
  #exit()
  
  return ac["auc"],ac["e30"],shape,ac

d1l=1
d1h=4
d2l=0
d2h=1
#First Particle/feature, First Particle/second feature...


#auc,e30,shape,data=test(*partiate(p,c,y,d1l,d1h,d2l,d2h))

if shallnorm:
  auc,e30,shape,data=testd(*xnorm(*partiate2(p,c,y,d)))
  auc_0,e30_0,shape_0,data_0=testd(*xnorm(p,c,y))
else:
  auc,e30,shape,data=test(*partiate2(p,c,y,d))
  auc_0,e30_0,shape_0,data_0=test(p,c,y)
  
#exit()

print("new",{"auc":auc,"1/e30":1/e30,"shape":shape})
print("old",{"auc":auc_0,"1/e30":1/e30_0,"shape":shape_0})


if "h" in key:
  d0=data["d0"]
  d1=data["d1"]

  #print("d0",np.mean(d0),np.std(d0))

  minx=np.min([np.min(d0),np.min(d1)])
  maxx=np.max([np.max(d0),np.max(d1)])
  rang=[minx,maxx]


  if "t" in key:
    plt.hist(np.concatenate((d0,d1)),bins=200)
  else:
    plt.hist(d0,bins=100,alpha=0.5,label="0",range=rang,color="orange")
    plt.hist(d1,bins=100,alpha=0.5,label="1",range=rang,color="blue")

    plt.legend()

  plt.xlabel("mse")
  plt.ylabel("#")




  plt.show()
  plt.close()

  


















