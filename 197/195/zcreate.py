import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization,Conv2D
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *
from gcomdex import *
from gcomjpool import *
from gcomgpool import *
from gchooseparam import *
from gcomdepoollg import *
from gcomdiagraph import *
from gcomreopool import *
from gcomparastract import *
from glam import *
from gcomdensemerge import *
from gcompoolmerge import *
from gcomgraphcutter import *
from gcomdensediverge import *
from gcomgraphfromparam import *
from gcomgraphcombinations import *
from gcomgraphand import *
from gcomgraphrepeat import *
from gcomgraphlevel import *
from gcomgraphand2 import *
from gcomparamlevel import *
from gliam import *
from gcomfullyconnected import *
from gcomparamcombinations import *
from gcomgraphfrom2param import *
from gcomextractdiag import *
from gmultiply import *
from dedge import *
from dexpand import  *
from dnarrow import *
from dtorb import *
from dreflag import *
from dtcreate import *
from dtdestroy import *
from glcreate import *
from glacreate import *

objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gltknd":gltknd,"gpre1":gpre1,"gpre2":gpre2,"gpre3":gpre3,"gpre4":gpre4,"gpre5":gpre5,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"gaddzeros":gaddzeros,"gsym":gsym,"gbrokengrowth":gbrokengrowth,"gpoolgrowth":gpoolgrowth,"gremoveparam":gremoveparam,"gcompool":gcompool,"gcomdepool":gcomdepool,"gvaluation":gvaluation,"ggoparam":ggoparam,"gfromparam":gfromparam,"gaddbias":gaddbias,"gssort":gssort,"glm":glm,"glom":glom,"glim":glim,"ggraphstract":ggraphstract,"gmake1graph":gmake1graph,"gcomdepoolplus":gcomdepoolplus,"gcomdex":gcomdex,"gcomjpool":gcomjpool,"gcomgpool":gcomgpool,"gchooseparam":gchooseparam,"gcomdepoollg":gcomdepoollg,"gcomdiagraph":gcomdiagraph,"gcomreopool":gcomreopool,"gcomparastract":gcomparastract,"glam":glam,"gcomdensemerge":gcomdensemerge,"gcompoolmerge":gcompoolmerge,"gcomgraphcutter":gcomgraphcutter,"gcomdensediverge":gcomdensediverge,"gcomgraphfromparam":gcomgraphfromparam,"gcomgraphcombinations":gcomgraphcombinations,"gcomgraphand":gcomgraphand,"gcomgraphrepeat":gcomgraphrepeat,"gcomgraphlevel":gcomgraphlevel,"gcomgraphand2":gcomgraphand2,"gcomparamlevel":gcomparamlevel,"gliam":gliam,"gcomfullyconnected":gcomfullyconnected,"gcomparamcombinations":gcomparamcombinations,"gcomgraphfrom2param":gcomgraphfrom2param,"gcomextractdiag":gcomextractdiag,"gmultiply":gmultiply,"dedge":dedge,"dexpand":dexpand,"dnarrow":dnarrow,"dtorb":dtorb,"dreflag":dreflag,"dtcreate":dtcreate,"dtdestroy":dtdestroy,"glcreate":glcreate,"glacreate":glacreate}



class state:
  gs=10
  param=10

  def __init__(self,gs,param):
    self.gs=gs
    self.param=param
  def __repr__(self):
    return str(self.gs)+"*"+str(self.param)
class grap:
  A=None
  X=None
  s=None
  def __init__(self,s):
    self.A=None
    self.X=None
    self.s=s
class setting:

  def __init__(self,**kwargs):
    pass
    #setattr(self,key,value)



def prep(g,m):#preparation handler, return g and input
  inp=Input(shape=(g.s.gs,4))
  feat0=gpre5(gs=g.s.gs)(inp)
  if m.prenorm:norm(g,scale=False)
  g.X=feat0
  g.s.param=4
  return g,inp


def norm(g,scale=True):
  g.X=BatchNormalization(axis=-1,scale=scale)(g.X)
  return g



def sortparam(g,m):
  g.X=gssort(gs=g.s.gs,param=g.s.param,index=m.sortindex)([g.X])
  g.A=None
  return g




def zcreate(gs,**kwargs):






  #not completely convinced that this actually does what i want (since main.py 0 1, also same spikes in other dataset)
  #missed some stuff (sort, norm), should be fixed

  #assert gs==6

  global m
  m=setting()
  m.usei=False
  m.decompress="trivial"

  m.trivial_ladder_n=5
  m.trivial_decompress_activation="relu"
  m.trivial_decompress_init_kernel=t.keras.initializers.TruncatedNormal()
  m.trivial_decompress_init_bias=t.keras.initializers.TruncatedNormal()

  m.sortindex=-1
  m.prenorm=False

  m.graph_init_self=t.keras.initializers.TruncatedNormal()
  m.graph_init_neig=t.keras.initializers.TruncatedNormal()
  m.agraph_init_self=m.graph_init_self
  m.agraph_init_neig=m.graph_init_neig

  m.edges=3#particle net like
  m.edgeactivation="relu"
  m.edgeactivationfinal=m.edgeactivation
  m.edgeusebias=False
  m.edgeconcat=False

  m.gq_activation="relu"
  m.gq_init_kernel=tf.keras.initializers.TruncatedNormal()
  m.gq_init_bias=tf.keras.initializers.Zeros()
  m.gq_usebias=False
  m.gq_batchnorm=False

  m.shallcomplex=not True
  m.complexsteps=3 

  m.gqa_activation=m.gq_activation
  m.gqa_init_kernel=m.gq_init_kernel
  m.gqa_init_bias=m.gq_init_bias
  m.gqa_usebias=m.gq_usebias
  m.gqa_batchnorm=m.gq_batchnorm

  m.shallacomplex=m.shallcomplex
  m.complexasteps=m.complexsteps

  m.shallredense=False
  m.redenseladder=[70,50]
  m.redenseactivation="relu"
  m.redenseinit=keras.initializers.Identity()

  m.compression_init=keras.initializers.Identity()


  c=4
  addparam=2


  s=state(gs=gs,param=None)
  g=grap(s)

  g,inn=prep(g,m)
  g=sortparam(g,m)
  return inn,g.X

def zageta(gs):
  inn,out=zcreate(gs=gs)
  return Model(inputs=inn,outputs=out)







