import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import isfile

from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.colors import LogNorm


from cauc import cauc
from ageta import gs


def raster():
  if isfile("data/twoarbite.npz"):
    f=np.load("data/twoarbite.npz")
    return np.reshape(f["x"],(gs,4))
  return [[0,0],[0,0]]


if True:
  loss=raster()
  print("raster")
  print("factors")
  print(loss)

  s1,s2=loss.shape

  xx,yy=np.mgrid[0:s2:1,0:s1:1]

  xx=xx.flatten()
  yy=yy.flatten()


  if True:
    plt.pcolor(loss,cmap="Blues")#,norm=LogNorm(vmin=np.min(loss), vmax=np.max(loss)))#,vmin=0.0,vmax=1.0)
    plt.title("mult")
    plt.ylabel("particle")
    plt.xlabel("feature")




    plt.savefig("imgs/multmap.png",format="png")
    plt.savefig("imgs/multmap.pdf",format="pdf")

    plt.show()
    plt.close() 

  


















