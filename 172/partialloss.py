import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import isfile

from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.colors import LogNorm


from cauc import cauc



N=100

cmap=np.zeros((2*N,4))

col1=np.array([255,0,68])
col2=np.array([0,21,255])

col1=col1/255
col2=col2/255

for i in range(N):
  alpha=1-i/N
  cmap[i]=[col1[0],col1[1],col1[2],alpha]
  cmap[2*N-i-1]=[col2[0],col2[1],col2[2],alpha]

cmap=ListedColormap(cmap)



fil="evalb"

key=""


if len(sys.argv)>1:
  key=sys.argv[1]


if "1" in key:fil="evalb"
if "2" in key:fil="evalb2"
if "3" in key:fil="evalb3"

idd=fil[4:]

d=[0]
shallsaved=False
if isfile("lastd.npz"):
  d=list(np.load("lastd.npz")["d"])
if len(sys.argv)>2:
  d=[]
  for i in range(2,len(sys.argv)):
    d.append(int(sys.argv[i]))
  shallsaved=True



f=np.load(fil+".npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]



if "p" in key:
  d=[]
  pn=int(c.shape[1])
  tn=int(c.shape[1])*int(c.shape[2])
  for i in range(tn-pn,tn):
    d.append(i)
  shallsaved=True

if "pp" in key:
  d=[]
  pn=int(c.shape[1])
  tn=int(c.shape[1])*int(c.shape[2])
  for i in range(tn-2*pn,tn):
    d.append(i)
  shallsaved=True

if "w" in key:
  d=[]
  pn=int(c.shape[1])
  tn=int(c.shape[1])*int(c.shape[2])
  for i in range(pn,3*pn):
    d.append(i)
  shallsaved=True

if "w" in key and "p" in key:
  d=[]
  pn=int(c.shape[1])
  tn=int(c.shape[1])*int(c.shape[2])
  for i in range(tn-3*pn,tn):
    d.append(i)
  shallsaved=True



if shallsaved:
  np.savez_compressed("lastd",d=d)

def getloss(p,c,y):
  return np.mean((p-c)**2)
def raster(p,c,y):
  if isfile("raster.npz") and "l" in key:
    f=np.load("raster.npz")
    return f["loss"]
  d1=int(p.shape[-2])
  d2=int(p.shape[-1])

  loss=np.zeros((d1,d2))


  for i1 in range(d1):
    for i2 in range(d2):
      ac=getloss(p=p[:,i1,i2],c=c[:,i1,i2],y=y)
      loss[i1,i2]=ac
      print("did",str(i1)+"/"+str(d1),str(i2)+"/"+str(d2))


  loss/=np.mean(loss,axis=0)


  np.savez_compressed("raster",loss=loss)
  return loss

def partiate(p,c,y,d1l,d1h,d2l,d2h):
  return p[:,d1l:d1h,d2l:d2h],c[:,d1l:d1h,d2l:d2h],y
def partiate2(p,c,y,d):
  ap=np.reshape(p,(p.shape[0],p.shape[1]*p.shape[2]),order="F")
  ac=np.reshape(c,(c.shape[0],c.shape[1]*c.shape[2]),order="F")
  return ap[:,d],ac[:,d],y
def test(p,c,y):
  ac=loss(p=p,c=c,y=y)
  return ac,p.shape,ac

if "r" in key:
  loss=raster(p,c,y)
  print("raster")
  print("loss")
  print(loss)

  s1,s2=loss.shape

  xx,yy=np.mgrid[0:s2:1,0:s1:1]

  xx=xx.flatten()
  yy=yy.flatten()


  if "a" in key:
    plt.pcolor(loss,cmap="Blues",norm=LogNorm(vmin=np.min(loss), vmax=np.max(loss)))#,vmin=0.0,vmax=1.0)
    plt.title("loss")
    plt.ylabel("particle")
    plt.xlabel("feature")

    if "m" in key:
      for ad in d:
        plt.plot([xx[ad]+0.5],[yy[ad]+0.5],"o",color="black")



    if "s" in key:plt.savefig("imgs/lossmap"+idd+".png",format="png")
    if "s" in key:plt.savefig("imgs/lossmap"+idd+".pdf",format="pdf")

    plt.show()
    plt.close() 

  


















