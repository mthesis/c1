import numpy as np
import matplotlib.pyplot as plt
import sys
import os

meanf="41"
meanf=""


job="debug"
if len(sys.argv)>1:job=sys.argv[1]


folders=[]
labels=[]

title=""
lowpow=2.5
lowpow2=lowpow

if job=="cs16":
  folders=["25","20","21","05","22","23"]
  labels=[3,6,10,12,16,24]
  title=None
  lowpow=2.5

if job=="cs64":
  folders=["06","07","08","09","10","11"]
  labels=["20 cpu","20","8","50","20 less data","20 less data, keep"]
  title=None
  lowpow=2.5

if job=="scale":
  folders=["00","05","09"]
  labels=["4","16","64"]
  title=None
  lowpow=2.5

if job=="cgraph16":
  folders=["12","13","15","16","17","18"]
  labels=["simple","no angles","learn degraph","only angle graph","learn degraph param=4","rounding degraph param=4"]
  title=None
  lowpow=2.5

if job=="dtop85":
  folders=["00","14"]
  labels=["qcd","top"]
  title=None
  lowpow=2.5

if job=="dtop16":
  folders=["16","19"]
  labels=["qcd","top"]
  title=None
  lowpow=2.5

if job=="scale29e":
  folders=["39","29","42","41","46","47","49"]#,"40"]
  labels=[4,6,9,12,-1,-2,-3]#,24]
  title="scale29e"
  lowpow=2.5

if job=="scale29":
  folders=["39","29","42","41"]#,"40"]
  labels=[4,6,9,12]#,24]
  title="scale29"
  lowpow=2.5

if job=="scale0":
  folders=["00","43","44","22"]
  labels=[4,6,10,16]
  title="scale0"
  lowpow=2.5

if job=="debug":
  folders=["48","32"]
  labels=["debug","real"]
  title="debug"
  lowpow=2.5

if job=="decode":
  folders=["32","36","35"]
  labels=["trivial","param","graph"]
  title="decode"
  lowpow=2.5

if job=="dual":
  folders=["51","52","53","54","55"]
  labels=["just","relu","sort","init","init relu"]
  title="dual"
  lowpow=2.5

if job=="super":
  folders=["45/super","59/super","60/super","61/super","62/super"]
  labels=[6,9,16,40,100]
  title="super"
  lowpow=2.5

if job=="trivial":
  folders=["49","46","56","57","58"]
  labels=[4,6,16,40,100]
  title="trivial"
  lowpow=2.5




if len(folders)!=len(labels):
  labels=folders



oldroc=False
brandnewroc=True

if len(sys.argv)>2:
  oldroc=bool(int(sys.argv[2]))




def int(x,y):
  x,y=(list(t) for t in zip(*sorted(zip(x,y))))
  ret=0.0
  for i in range(1,len(x)):
    ret+=((y[i]+y[i-1])*(x[i]-x[i-1]))/2
  return ret

fs=9
fig,(ax2,ax1)=plt.subplots(2,figsize=(fs,fs*(3/4)))

if len(meanf)>0:
  fm=np.load(meanf+"/meanroc.npz")
  meanauc=int(1-fm["fpr"],1-fm["tpr"])
  ax2.axhline(meanauc,alpha=0.5,color="orange")


ax1.axvline(0.3,alpha=0.3,color="grey")

for folder,label in zip(folders,labels):
  if os.path.isfile(folder+"/roc.npz"):
    f=np.load(folder+"/roc.npz")
  elif os.path.isfile(folder+"roc.npz"):
    f=np.load(folder+"roc.npz")
  else:
    f=np.load(folder)

  col=None


  if "nw" in f.files:
    fpr=f["fpr"]#1- because of invert
    tpr=f["tpr"]
  else:
    fpr=1-f["fpr"]
    tpr=1-f["tpr"]
    col="red"
  auc=int(fpr,tpr)
  
  if oldroc:
    ax1.plot(fpr,tpr,label=str(label)+": "+str(round(auc,4)),color=col)
  elif brandnewroc:
    ax1.plot(tpr,1/(fpr+0.00001),label=str(label)+": "+str(round(auc,4)),color=col)
  else:
    ax1.plot(tpr,fpr,label=str(label)+": "+str(round(auc,4)),color=col)

  if not(job=="best" and auc<0.5):ax2.plot(label,auc,"o")

  print(label,":",auc)

if oldroc:
  ax1.set_xlabel("fpr")
  ax1.set_ylabel("tpr")
  ax1.legend(loc=4)
elif brandnewroc:
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlim([-0.05,1])
  ax1.set_ylim((1,10**(lowpow2)))
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlabel("true positive rate")
  ax1.set_ylabel("1/false positive rate")
  ax1.legend(loc=1)
else:
  ax1.set_xlim([0,1])
  ax1.set_ylim((10**(-lowpow),1))
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_xlabel("fpr")
  ax1.set_ylabel("tpr")
  ax1.legend(loc=4)

ax2.set_ylabel("auc")

fig.suptitle(title)


plt.savefig("images/"+job+".png",format="png")
plt.savefig("images/"+job+".pdf",format="pdf")




plt.show()










